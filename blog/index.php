<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="centeredContent">
        <h1>Welcome to my blog, read on!</h1>
        <?php
        session_start();
        require_once 'db.php';
        $query = sprintf("SELECT articles.id artId,articles.title,articles.body,users.username,articles.creationTime"
                . " FROM articles INNER JOIN users ON articles.authorId=users.id");
        $result = mysqli_query($link, $query);
         if (!$result) {
                echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                exit;
            }

        foreach ($result as $article) {
            
            $id=$article['artId'];
           
            echo "<h3><a href=article.php?id=$id>" . $article['title'] . "</a></h3><br>";
            echo "<small>Posted by " . $article['username'] . "</small> on " . $article['creationTime'];
            $preview = substr($article["body"], 0, 200);
            echo "<p>$preview</p>";
        }
        ?>
        </div>>
    </body>
</html>

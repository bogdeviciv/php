<?php

require_once 'vendor/autoload.php';


DB::$user = "first";
DB::$dbName = "first";
DB::$password = "OXF8wphdhjnFZRmT";
DB::$port = 3333;
DB::$host = 'localhost';
DB::$encoding = "utf8";


// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');


/*
  $app->get('/hello/:name', function ($name) {
  echo "Hello, " . $name;
  });


  $app->get('/hello/:name/:age', function ($name,$age) use ($app) {

  DB::insert('people',array(
  'name'=>$name,
  'age'=>$age
  ));
  $app->render('hello.html.twig', array(
  'name'=>$name,
  'age'=>$age
  ));
  });

  $app->get('/list', function (){
  $peopleList=DB::query('SELECT * FROM people');
  print_r($peopleList);
  }
  );


 */

$app->get('/addme', function () use ($app) {
    //state 1 : first sfow
    $app->render('addme.html.twig');
});

$app->post('/addme', function () use ($app) {
    // receiving submission
    $name = $app->request()->post('name');
    $age = $app->request()->post('age');
    //verify submission
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorList, "Name must be 2-100 ch long");
    }
    if (!is_numeric($age) || $age < 1 || $age > 150) {
        array_push($errorList, "Age must be 1-150");
    }

    if (!$errorList) {
        // state 2: succes
        DB::insert('people', array(
            'name' => $name,
            'age' => $age
        ));
        $app->render("addme_succes.html.twig");
    } else {
        // state3 :  fail
        $app->render('addme.html.twig', array(
            'errorList' => $errorList));
    }
});







$app->get('/random', function () use ($app) {
    //state 1 : first sfow
    $app->render('random.html.twig');
});

$app->post('/random', function () use ($app) {
    // receiving submission
    $min = $app->request()->post('min');
    $max = $app->request()->post('max');
    $count = $app->request()->post('count');
    //verify submission
    $errorList = array();

    if (!is_numeric($min) || $min < 1) {
        array_push($errorList, "Min must be a number greater or equal to 1");
    }
    if (!is_numeric($max) || $max < 1) {
        array_push($errorList, "Max must be a number greater or equal to 1");
    }
    if ($min > $max) {
        array_push($errorList, "Min must be equal or smaller than max");
    }
    if ($count < 1) {
        array_push($errorList, "Count must be 1 or more");
    }

    if (!$errorList) {
        // state 2: succes
        $numbers = array();
        for ($i = 0; $i < $count; $i++) {
            $number = rand($min, $max);
            array_push($numbers, $number);
        }

        $app->render("random.html.twig", array(
            'numbers' => $numbers));
    } else {
        // state3 :  fail
        $app->render('random.html.twig', array(
            'errorList' => $errorList));
    }
});

$app->run();

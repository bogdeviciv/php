<?php

/* addme.html.twig */
class __TwigTemplate_40656dabc8347b69e3518ac80a90c79ea6b6528dbb9684f00eb4f258bd8e6621 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        if (($context["errorList"] ?? null)) {
            // line 4
            echo "    <ul>
        ";
            // line 5
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 6
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "    </ul>
";
        }
        // line 10
        echo "
<form method=\"post\">
    Name:  <input type=\"text\" name=\"name\"><br>
    Age:   <input type=\"number\" name=\"age\"><br>
    <input type=\"submit\" value=\"ADD\">
</form>

";
    }

    public function getTemplateName()
    {
        return "addme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 10,  41 => 8,  32 => 6,  28 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("

{% if errorList %}
    <ul>
        {% for error in errorList %}
            <li>{{error}}</li>
        {% endfor %}
    </ul>
{% endif %}

<form method=\"post\">
    Name:  <input type=\"text\" name=\"name\"><br>
    Age:   <input type=\"number\" name=\"age\"><br>
    <input type=\"submit\" value=\"ADD\">
</form>

", "addme.html.twig", "C:\\xampp\\htdocs\\php\\slimfirst\\templates\\addme.html.twig");
    }
}

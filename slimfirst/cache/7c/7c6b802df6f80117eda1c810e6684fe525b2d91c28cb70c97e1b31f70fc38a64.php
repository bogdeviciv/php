<?php

/* random.html.twig */
class __TwigTemplate_3502679b375060cc22eef6a72ac123c899952421238de877b38fc3fe32ca7a56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        if (($context["errorList"] ?? null)) {
            // line 3
            echo "    <ul>
        ";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 5
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "    </ul>
";
        }
        // line 9
        echo "
";
        // line 10
        if (($context["numbers"] ?? null)) {
            // line 11
            echo "    <p>
        ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["numbers"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["number"]) {
                // line 13
                echo "            ";
                echo twig_escape_filter($this->env, $context["number"], "html", null, true);
                echo ",
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['number'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "    </p>
";
        }
        // line 17
        echo "<form method=\"post\">
    Min:  <input type=\"text\" name=\"min\"><br>
    Max:   <input type=\"number\" name=\"max\"><br>
    Count:   <input type=\"number\" name=\"count\"><br>
    <input type=\"submit\" value=\"ADD\">
</form>
 
";
    }

    public function getTemplateName()
    {
        return "random.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 17,  65 => 15,  56 => 13,  52 => 12,  49 => 11,  47 => 10,  44 => 9,  40 => 7,  31 => 5,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% if errorList %}
    <ul>
        {% for error in errorList %}
            <li>{{error}}</li>
        {% endfor %}
    </ul>
{% endif %}

{% if numbers %}
    <p>
        {% for number in numbers %}
            {{ number}},
        {% endfor %}
    </p>
{% endif %}
<form method=\"post\">
    Min:  <input type=\"text\" name=\"min\"><br>
    Max:   <input type=\"number\" name=\"max\"><br>
    Count:   <input type=\"number\" name=\"count\"><br>
    <input type=\"submit\" value=\"ADD\">
</form>
 
", "random.html.twig", "C:\\xampp\\htdocs\\php\\slimfirst\\templates\\random.html.twig");
    }
}

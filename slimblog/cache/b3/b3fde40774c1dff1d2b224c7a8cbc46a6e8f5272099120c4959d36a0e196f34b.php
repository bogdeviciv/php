<?php

/* master.html.twig */
class __TwigTemplate_4af65d0d1ace238c8af7f5a52ea00ee689db0387988a7eb2638699586e12b2c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"styles.css\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo " - My Webpage</title>
        ";
        // line 6
        $this->displayBlock('addhead', $context, $blocks);
        // line 9
        echo "    </head>
    <body>
        <div id=\"centeredContent\">";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "            <div id=\"header\">
               ";
        // line 13
        if (($context["sessionUSER"] ?? null)) {
            // line 14
            echo "                   You are logged in as ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["sessionUser"] ?? null), "email", array()), "html", null, true);
            echo ", you can 
                   <a href=\"/logout\">logout</a> or <a href=\"/articleadd\">post an article</a> 
                   ";
        } else {
            // line 17
            echo "                       You can <a href=\"/login\">login</a> or <a href=\"/register\">register</a>
                       ";
        }
        // line 19
        echo "            </div>
            <div id=\"footer\">&copy; Copyright 2011 by <a href=\"http://domain.invalid/\">you</a>.</div>
        </div>
    </body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 6
    public function block_addhead($context, array $blocks = array())
    {
        // line 7
        echo "
        ";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 11,  72 => 7,  69 => 6,  64 => 5,  56 => 19,  52 => 17,  45 => 14,  43 => 13,  40 => 12,  38 => 11,  34 => 9,  32 => 6,  28 => 5,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"styles.css\" />
        <title>{% block title %}{% endblock %} - My Webpage</title>
        {% block addhead %}

        {% endblock %}
    </head>
    <body>
        <div id=\"centeredContent\">{% block content %}{% endblock %}
            <div id=\"header\">
               {% if sessionUSER %}
                   You are logged in as {{sessionUser.email}}, you can 
                   <a href=\"/logout\">logout</a> or <a href=\"/articleadd\">post an article</a> 
                   {% else  %}
                       You can <a href=\"/login\">login</a> or <a href=\"/register\">register</a>
                       {% endif %}
            </div>
            <div id=\"footer\">&copy; Copyright 2011 by <a href=\"http://domain.invalid/\">you</a>.</div>
        </div>
    </body>
</html>{# empty Twig template #}
", "master.html.twig", "C:\\xampp\\htdocs\\php\\slimblog\\templates\\master.html.twig");
    }
}

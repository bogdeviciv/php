<?php

/* login.html.twig */
class __TwigTemplate_a88c3403678bcf16ead73a383a773ad10afdf11571cf29ee0917f1b8f6d58a63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 3);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        // line 6
        echo "    Login 
";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "<form method=\"post\">
    Username: <input type=\"text\" name=\"username\" ><br>
    Password <input type=\"password\" name=\"password\"><br>
    <input type=\"submit\" value=\"Login\">
</form> 

";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 10,  37 => 9,  32 => 6,  29 => 5,  11 => 3,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("

{% extends \"master.html.twig\" %}

{% block title %}
    Login 
{% endblock %}

{% block content %}
<form method=\"post\">
    Username: <input type=\"text\" name=\"username\" ><br>
    Password <input type=\"password\" name=\"password\"><br>
    <input type=\"submit\" value=\"Login\">
</form> 

{% endblock %} ", "login.html.twig", "C:\\xampp\\htdocs\\php\\slimblog\\templates\\login.html.twig");
    }
}

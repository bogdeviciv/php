<?php

/* register.html.twig */
class __TwigTemplate_f7e257a1a833bf21677b6177ffa85ab29287b030c5e1db1c8d9cc8c9d1fb458e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 2);
        $this->blocks = array(
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        // line 5
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"input[name=email]\").keyup(function () {
                var email = \$(\"input[name=email]\").val();
                \$(\"#emailTaken\").load(\"isemailregistered/\" + email);
            });
        });
    </script>
";
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        echo "    ";
        if (($context["errorList"] ?? null)) {
            // line 19
            echo "        <ul>
            ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 21
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "        </ul>
    ";
        }
        // line 25
        echo "


    <form method=\"post\">
        Email:  <input type=\"text\" name=\"email\" value=";
        // line 29
        echo twig_escape_filter($this->env, ($context["email"] ?? null), "html", null, true);
        echo "><span id=\"emailTaken\"></span><br>
        Password:  <input type=\"password\" name=\"pass1\"><br>  
        Password confirm:    <input type=\"password\" name=\"pass2\"><br>
        <input type=\"submit\" value=\"Register\">
        
    </form>";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 29,  71 => 25,  67 => 23,  58 => 21,  54 => 20,  51 => 19,  48 => 18,  45 => 17,  32 => 5,  29 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends \"master.html.twig\" %}

{% block addhead %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"input[name=email]\").keyup(function () {
                var email = \$(\"input[name=email]\").val();
                \$(\"#emailTaken\").load(\"isemailregistered/\" + email);
            });
        });
    </script>
{% endblock %}


{% block content %}
    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}



    <form method=\"post\">
        Email:  <input type=\"text\" name=\"email\" value={{email}}><span id=\"emailTaken\"></span><br>
        Password:  <input type=\"password\" name=\"pass1\"><br>  
        Password confirm:    <input type=\"password\" name=\"pass2\"><br>
        <input type=\"submit\" value=\"Register\">
        
    </form>{# empty Twig template #}
{% endblock %}", "register.html.twig", "C:\\xampp\\htdocs\\php\\slimblog\\templates\\register.html.twig");
    }
}

<?php
session_start();
require_once 'vendor/autoload.php';

DB::$user = 'slimblog';
DB::$dbName = 'slimblog';
DB::$password = 'kyZ1iDJZ13F7QfOm';
DB::$port = 3333;
DB::$host = 'localhost';
DB::$encoding = 'utf8';

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

if(!isset($_SESSION['user'])){
    $_SESSION['user']=array();
}

$twig=$app->view()->getEnvironment();
$twig->addGlobal('sessionUser', $_SESSION['user']);

$app->get('/', function() use ($app) {
    $articleList = DB::query("SELECT a.id, a.creationTime, a.title, a.body, u.email authorName " .
                " FROM articles as a, users as u WHERE a.authorId = u.id");
    $app->render('index.html.twig', array('al' => $articleList));
});

$app->get('/article/:id', function($id) use ($app) {
    $article = DB::queryFirstRow("SELECT a.id, a.creationTime, a.title, a.body, u.email authorName " .
                " FROM articles as a, users as u WHERE a.authorId = u.id AND a.id=%s", $id);
    $app->render("article_view.html.twig", array('a' => $article));
});

$app->get('/articleadd', function() use ($app) {
    // state 1: first show
    if (!isset($_SESSION['user'])) {
        $app->render('access_denied.html.twig');
        return;
    }
    $app->render('article_add.html.twig');
});

$app->post('/articleadd', function() use ($app) {
    if (!isset($_SESSION['user'])) {
        $app->render('access_denied.html.twig');
        return;
    }
    // receiving submission
    $title = $app->request()->post('title');
    $body = $app->request()->post('body');
    $valueList = array('title' => $title, 'body' => $body);
    // verify submission
    $errorList = array();
    if (strlen($title) < 2 || strlen($title) > 200) {
        array_push($errorList, "Title must be between 2-200 characters long");
    }
    if (strlen($body) < 2 || strlen($body) > 10000) {
        array_push($errorList, "Article body must be between 2-10000 characters long");
    }
    //
    if (!$errorList) {
        // state 2: successful submission
        DB::insert('articles', array(
            'authorId' => $_SESSION['user']['id'],
            'title' => $title,
            'body' => $body
        ));
        $articleId = DB::insertId();
        $app->render('article_add_success.html.twig', array('articleId' => $articleId));
    } else {
        // state 3: failed submission
        $app->render('article_add.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
                ));
    }
});

$app->get('/isemailregistered/(:email)', function($email = "") use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    echo ($user) ? "Email already in use" : "";
});

$app->get('/register', function() use ($app) {
    // state 1: first show
    $app->render('register.html.twig');
});

$app->post('/register', function() use ($app) {
    // receiving submission
    $email = $app->request()->post('email');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    $valueList = array('email' => $email);
    // verify submission
    $errorList = array();
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        array_push($errorList, "Email is invalid");
        unset($valueList['email']);
    }
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match");
    } else {
        if (strlen($pass1) < 6) {
            array_push($errorList, "Password must be at least 6 characters long");
        }
    }
    //
    if (!$errorList) {
        // state 2: successful submission
        DB::insert('users', array(
            'email' => $email,
            'password' => $pass1
        ));
        $app->render('register_success.html.twig');
    } else {
        // state 3: failed submission
        $app->render('register.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
                ));
    }
});

$app->get('/login', function() use ($app) {
    // state 1: first show
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app) {
    // receiving submission
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');
    // verify submission
    $isLoginSuccessful = false;
    //
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);    
    if ($user && ($user['password'] == $password)) {
        $isLoginSuccessful = true;
    }
    //
    if ($isLoginSuccessful) {
        // state 2: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        $app->render('login_success.html.twig',array('sessioUser'=>$_SESSION['user']));
    } else {
        // state 3: failed submission
        $app->render('login.html.twig', array('error' => true));
    }
});

$app->get('/logout', function() use ($app) {
    $_SESSION['user']=array();
    $app->render('logout.html.twig',array('sessioUser'=>$_SESSION['user']));
});

$app->get('/session', function() {
    echo "<pre>SESSION:\n\n";
    // var_dump($_SESSION);
    print_r($_SESSION);
});


$app->run();



<?php

session_start();
require_once 'vendor/autoload.php';

// MONOLOG USAGE
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
///////////////////////////////////////


DB::$user = "slimtest";
DB::$dbName = "slimtest";
DB::$password = "3QQmHR7ql4Si8q9b";
DB::$port = 3333;
DB::$host = 'localhost';
DB::$encoding = "utf8";

DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $app, $log;
    $log->error("Error: " . $params['error']);
    $log->error("Query: " . $params['query']);
    http_response_code(500);
    $app->render('fatal_error.html.twig');
    die;
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');
\Slim\Route::setDefaultConditions(array('id'=>'/d+'));




$app->get('/products/:id', function($id) use ($app, $log) {
    $product=DB::query('SELECT * from products WHERE id=%i',$id);
    if(!$product){
        $app->notFound();
        return;
    }
    $app->render('product_view.html.twig', array('p'=>$product));
});


$app->get('/products/add', function() use ($app, $log) {
    $app->render('product_add.html.twig');
});





$app->post('/products/add', function() use ($app, $log) {
    $name = $app->request()->post('name');
    $price = $app->request()->post('price');
    $description = $app->request()->post('description');
    $valueList = array('name' => $name,
        'price' => $price,
        'description' => $description);
    
    $errorList=array();
    if(strlen($name)<2 || strlen($name)>100){
        array_push($errorList, "Name must be 2-100 characters");      
    }
    
    if(strlen($description)<2 || strlen($description)>2000){
        array_push($errorList, "Description must be 2-2000 characters");      
    }
    
     if(!is_numeric($price) || $price<0 || $price>999999.99){
        array_push($errorList, "Price must be 0-999999.99");      
    }
    
    $image=$_FILES['image'];
    //check if it is an image
    $imageInfo= getimagesize($image['tmp_name']);
    if(!$imageInfo){
        array_push($errorList, "File is not a valid image");
    }else
    {
        //do not allow .. in the file name
        if(strstr($image['name'], '..')){
            array_push($errorList, "Image name not valid");
            
        }
        // only allow selected extensions
        $ext= strtolower(pathinfo($image['name'],PATHINFO_EXTENSION));
        if(!in_array($ext, array('jpg','jpeg','gif','png'))){
            array_push($errorList, "File extension invalid");
            
        }
        // do not allow to override existing files
        if(file_exists('uploads/'.$image['name'])){
            array_push($errorList, "File name already exists");
        }
        
    }
    if($errorList){
        $app->render('product_add.html.twig',array('v'=>$valueList,'errorList'=>$errorList));
        
    } else {
        move_uploaded_file($image['tmp_name'], 'uploads/'.$image['name']);
        DB::insert('products',array(
            'name'=>$name,
            'description'=>$description,
            'price'=>$price,
            'imagePath'=>$image['name']
        ));
        $productId=DB::insertId();
        $app->render('product_add_succes.html.twig',array('productId'=>$productId));
    }
    
    
    
});








$app->get('/passport/:id', function($id) use ($app, $log) {
    $product=DB::query('SELECT * from products WHERE id=%i',$id);
    if(!$product){
        $app->notFound();
        return;
    }
    $app->render('passport_view.html.twig', array('p'=>$product));
});


$app->get('/passport/add', function() use ($app, $log) {
    $app->render('passport_add.html.twig');
});





$app->post('/passport/add', function() use ($app, $log) {
    $name = $app->request()->post('name');
    $number = $app->request()->post('number');
        $valueList = array('name' => $name,
        'number' => $number);
    
    $errorList=array();
    if(strlen($name)<2 || strlen($name)>100){
        array_push($errorList, "Name must be 2-100 characters");      
    }
    
    if(strlen($number)<2 || strlen($number)>2000){
        array_push($errorList, "Description must be 2-2000 characters");      
    }
    
     
    
    $image=$_FILES['image'];
    //check if it is an image
    $imageInfo= getimagesize($image['tmp_name']);
    if(!$imageInfo){
        array_push($errorList, "File is not a valid image");
    }else
    {
        //do not allow .. in the file name
        if(strstr($image['name'], '..')){
            array_push($errorList, "Image name not valid");
            
        }
        // only allow selected extensions
        $ext= strtolower(pathinfo($image['name'],PATHINFO_EXTENSION));
        if(!in_array($ext, array('jpg','jpeg','gif','png'))){
            array_push($errorList, "File extension invalid");
            
        }
        
        $mimeType=$imageInfo['mime'];
        if(!in_array($mimeType, array('image/gif','image/jpeg','image/png'))){
            array_push($errorList, "File type invalid");
        }
       
        
        
    }
    if($errorList){
        $app->render('passport_add.html.twig',array('v'=>$valueList,'errorList'=>$errorList));
        
    } else {
        //move_uploaded_file($image['tmp_name'], 'uploads/'.$image['name']);
        $imageData= file_get_contents($image['tmp_name']);
        DB::insert('passports',array(
            'name'=>$name,
            'number'=>$number,
            'image'=>$imageData,
            'mimeType'=>$mimeType
        ));
        $passportId=DB::insertId();
        $app->render('passport_add_succes.html.twig',array('passportId'=>$passportId));
    }
    
    
    
});

$app->run();


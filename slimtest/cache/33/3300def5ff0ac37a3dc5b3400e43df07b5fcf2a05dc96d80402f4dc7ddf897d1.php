<?php

/* product_view.html.twig */
class __TwigTemplate_40e9e6ee04b6f2332d57a31098112fff9a1bdb9b1bbef30382c38b94fe9366f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product_view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "name", array()), "html", null, true);
        echo " -Products";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<div >
    <h3>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "namemmm", array()), "html", null, true);
        echo "</h3>
    <br><p>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "description", array()), "html", null, true);
        echo "</p><br>
    <b>Price:";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "price", array()), "html", null, true);
        echo "</b>
    <img scr=\"/uploads/";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["p"] ?? null), "image", array()), "html", null, true);
        echo "\" alt=\"img\"/>
    
    <div>";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["a"] ?? null), "body", array()), "html", null, true);
        echo "</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "product_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 12,  54 => 10,  50 => 9,  46 => 8,  42 => 7,  39 => 6,  36 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}{{p.name}} -Products{% endblock %}

{% block content %}
<div >
    <h3>{{p.namemmm}}</h3>
    <br><p>{{p.description}}</p><br>
    <b>Price:{{p.price}}</b>
    <img scr=\"/uploads/{{p.image}}\" alt=\"img\"/>
    
    <div>{{a.body}}</div>
</div>

{% endblock %}", "product_view.html.twig", "C:\\xampp\\htdocs\\php\\slimtest\\templates\\product_view.html.twig");
    }
}

<?php

/* passport_add_succes.html.twig */
class __TwigTemplate_396aee733aad1aa3cef208ebb80137f3ec529a7cd643da85ecf59eb76e9c68dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "passport_add_succes.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Success - Product added";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
<p>Product added <a href=\"/products/";
        // line 7
        echo twig_escape_filter($this->env, ($context["productId"] ?? null), "html", null, true);
        echo "\">Click here to view</a>.</p>

";
    }

    public function getTemplateName()
    {
        return "passport_add_succes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Success - Product added{% endblock %}

{% block content %}

<p>Product added <a href=\"/products/{{productId}}\">Click here to view</a>.</p>

{% endblock %}
", "passport_add_succes.html.twig", "C:\\xampp\\htdocs\\php\\slimtest\\templates\\passport_add_succes.html.twig");
    }
}

<?php

/* product_add.html.twig */
class __TwigTemplate_bae62c0c2438b0403712a145ae45f8c34398a572248458eff0443a607ab4722d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product_add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Add Article";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
";
        // line 8
        if (($context["errorList"] ?? null)) {
            echo "    
    <ul>
    ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "        <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "    </ul>
";
        }
        // line 15
        echo "
<form method=\"post\" enctype=\"multipart/form-data\">
    Name: <input type=\"text\" name=\"name\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "name", array()), "html", null, true);
        echo "\"><br>
    Description: <textarea name=\"description\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "description", array()), "html", null, true);
        echo "</textarea>
    Price: <input type=\"number\" name=\"price\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "price", array()), "html", null, true);
        echo "\"><br>
    Image: <input type=\"file\" name=\"image\" ><br>
    
    <input type=\"submit\" value=\"Add product\">
</form>
    
";
    }

    public function getTemplateName()
    {
        return "product_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 19,  71 => 18,  67 => 17,  63 => 15,  59 => 13,  50 => 11,  46 => 10,  41 => 8,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Add Article{% endblock %}

{% block content %}

{# empty Twig template #}
{% if errorList %}    
    <ul>
    {% for error in errorList %}
        <li>{{error}}</li>
    {% endfor %}
    </ul>
{% endif %}

<form method=\"post\" enctype=\"multipart/form-data\">
    Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br>
    Description: <textarea name=\"description\">{{v.description}}</textarea>
    Price: <input type=\"number\" name=\"price\" value=\"{{v.price}}\"><br>
    Image: <input type=\"file\" name=\"image\" ><br>
    
    <input type=\"submit\" value=\"Add product\">
</form>
    
{% endblock %}
", "product_add.html.twig", "C:\\xampp\\htdocs\\php\\slimtest\\templates\\product_add.html.twig");
    }
}

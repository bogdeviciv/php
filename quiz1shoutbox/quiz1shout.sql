-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jan 10, 2019 at 06:26 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz1shout`
--

-- --------------------------------------------------------

--
-- Table structure for table `shouts`
--

CREATE TABLE `shouts` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `message` varchar(100) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shouts`
--

INSERT INTO `shouts` (`id`, `name`, `message`, `ts`) VALUES
(1, 'victoria', 'hello', '2019-01-10 16:41:09'),
(2, 'victoria', 'hello', '2019-01-10 16:41:26'),
(3, 'jerry', 'qqqqqq', '2019-01-10 16:51:06'),
(4, 'aadada', 'qqqqqq', '2019-01-10 16:51:10'),
(5, '444', 'hello', '2019-01-10 16:51:15'),
(6, 'ana', 'bgbgbgbgbg', '2019-01-10 16:51:23'),
(7, 'jerry', 'fffff', '2019-01-10 16:51:26'),
(8, 'fffff', 'hhhhhhh', '2019-01-10 16:51:29'),
(9, 'ffffffffff', 'rgggrrgg', '2019-01-10 16:51:34'),
(10, 'ddddddd', 'ddddddd', '2019-01-10 16:51:42'),
(11, '444', 'hhhhhhh', '2019-01-10 16:51:57'),
(12, '444', 'hhhhhhh', '2019-01-10 16:55:38'),
(13, '444', 'hhhhhhh', '2019-01-10 16:56:06'),
(14, 'jerry', 'new', '2019-01-10 16:56:48'),
(15, 'victoria', 'hello hello', '2019-01-10 16:57:06'),
(16, 'jerry', 'bgbgbgbgbg', '2019-01-10 16:57:16'),
(17, 'ana', 'vvvvv', '2019-01-10 16:57:33'),
(18, 'ana', 'nhhhhh', '2019-01-10 16:57:38'),
(19, 'jerry', 'hello', '2019-01-10 17:04:29'),
(20, 'aadada', 'fffff', '2019-01-10 17:04:35'),
(21, 'jerry', 'hhhhhhh', '2019-01-10 17:05:01'),
(22, 'jerry', 'hello', '2019-01-10 17:06:45'),
(23, 'jerry', 'bgbgbgbgbg', '2019-01-10 17:06:49'),
(24, 'jerry', 'bgbgbgbgbg', '2019-01-10 17:10:21'),
(25, 'jerry', 'bgbgbgbgbg', '2019-01-10 17:10:28'),
(26, 'ana', 'hello', '2019-01-10 17:10:32'),
(27, 'je', 'fff', '2019-01-10 17:10:45'),
(28, 'jerry', 'hi', '2019-01-10 17:23:22'),
(29, 'jerry', 'hello', '2019-01-10 17:23:24'),
(30, 'victoria', 'qqqqqq', '2019-01-10 17:23:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shouts`
--
ALTER TABLE `shouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shouts`
--
ALTER TABLE `shouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

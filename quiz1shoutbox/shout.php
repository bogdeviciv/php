<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">       
        <link href="styles.css" rel="stylesheet">
        <title>Shout</title>
    </head>
    <body>


        <?php
        require_once 'db.php';
        
        function getForm($name = "", $message = "") {
            $form = <<< MARKER
<form method="post">
    Name: <input type="text" name="name" value="$name"><br>
            <span id="usernameTaken"></span><br>
    Message: <input type="text" name="message" value="$message"><br>   
    <input type="submit" value="Shout">
</form> 
MARKER;
            return $form;
        }

        if (isset($_POST['name'])) { // State 2 and 3 - receiving submission
            $name = $_POST['name'];
            $message = $_POST['message'];

            $errorList = array();
            //
            if (strlen($name) < 2 || strlen($name) > 20) {
                array_push($errorList, "Name must be 2-20 characters long");
            } else {
                if (strlen($message) < 1 || strlen($message) > 100) {
                    array_push($errorList, "Message must be 1-100 characters long");
                } else {
                    if (preg_match('/^[a-zA-Z0-9\s_]+$/', $name) != 1) {
                        array_push($errorList, "name must be composed of lower case letters and numbers,spaces or underscores");
                    }
                }
            }

            //
            if ($errorList) { // state 3: errors
                echo "<h3>Problems detected</h3>";
                echo "<ul>\n";
                foreach ($errorList as $error) {
                    echo "<li>" . $error . "</li>\n";
                }
                echo "</ul>\n";
                echo getForm($name, $message);
            } else { // state 2: submission successful
                // insert record into users table
                $query = sprintf("INSERT INTO shouts VALUES (NULL, '%s', '%s', NULL)", mysqli_real_escape_string($link, $name), mysqli_real_escape_string($link, $message));
                $result = mysqli_query($link, $query);
                
                
                if (!isset($_SESSION['count'])) {
                    $_SESSION['count'] = 0;
                }
                $_SESSION['count'] ++;

                
                
                if (!$result) {
                    echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
                    exit;
                }
                echo getForm($name);
            }
        } else { // state 1: first show
            echo getForm();
        }

        // get the recent shouts
        $query = sprintf("SELECT * FROM shouts ORDER BY id DESC LIMIT 10");
        $result = mysqli_query($link, $query);
        if (!$result) {
            echo "<p>Error: SQL database query error: " . mysqli_error($link) . "</p>";
            exit;
        }
        while ($shout = mysqli_fetch_assoc($result)) {
            $name = $shout['name'];
            $message = $shout['message'];
            $ts = $shout['ts'];

            echo "<p>On $ts $name shouted : $message";
        }
        $count = $_SESSION['count'];
        echo "<h5>You shouted $count times in this session</h5>";
        ?>

    </body>
</html>

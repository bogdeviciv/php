<?php

/* index.html.twig */
class __TwigTemplate_e707f1d4b34f152f7b13ce8b6b6e15ae909a099529e0f4ef32e9ad78085af5da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    Todos
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
<table class=\"table\">
    <thead class=\"thead-dark\">
        <th>Task</th>
        <th>DueDate</th>
        <th>isDone</th>
        
    </thead>
";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["todoList"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["todo"]) {
            // line 17
            echo "    <tr>
        <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "task", array()), "html", null, true);
            echo "</td>
        <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "dueDate", array()), "html", null, true);
            echo "</td>
        <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "isDone", array()), "html", null, true);
            echo "</td>
        
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['todo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "    
</table>

";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 24,  65 => 20,  61 => 19,  57 => 18,  54 => 17,  50 => 16,  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}
    Todos
{% endblock %}

{% block content %}

<table class=\"table\">
    <thead class=\"thead-dark\">
        <th>Task</th>
        <th>DueDate</th>
        <th>isDone</th>
        
    </thead>
{% for todo in todoList %}
    <tr>
        <td>{{ todo.task }}</td>
        <td>{{ todo.dueDate }}</td>
        <td>{{ todo.isDone }}</td>
        
    </tr>
    {% endfor %}
    
</table>

{% endblock %}{# empty Twig template #}
{# empty Twig template #}
", "index.html.twig", "C:\\xampp\\htdocs\\php\\slimtodo\\templates\\index.html.twig");
    }
}

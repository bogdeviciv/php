<?php

/* fatal_error.html.twig */
class __TwigTemplate_1dc0a480b3301fb712ab2f2e52e0c1d5c8760ecbc8e67b66c216e98e9d3fa5ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "fatal_error.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    Todos
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<img src=\"/img/fatal.jpg\" alt=\"fatal_error\" height/>

";
    }

    public function getTemplateName()
    {
        return "fatal_error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}
    Todos
{% endblock %}

{% block content %}
<img src=\"/img/fatal.jpg\" alt=\"fatal_error\" height/>

{% endblock %}", "fatal_error.html.twig", "C:\\xampp\\htdocs\\php\\slimtodo\\templates\\fatal_error.html.twig");
    }
}
